Dependencies: whatever package contains `libavformat`, `libavcodec` etc.. on your distro (Probably something like `ffmpeg-devel`)

Build requirements: Rust, `cargo`, `gcc`, `clang`


```
cargo build --release
# the resulting executable is target/release/cryptocam

# alternatively, to copy the executable to ~/cargo/bin
cargo install --path .
```

```
USAGE:
    cryptocam [OPTIONS] <SUBCOMMAND>

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information

OPTIONS:
    -k, --keyring <keyring>    Path to Cryptocam keyring directory (Default $HOME/CryptocamKeyring)

SUBCOMMANDS:
    decrypt       Decrypt Cryptocam output file
    delete-key    Delete key
    export-key    Export public key to recipient file
    help          Prints this message or the help of the given subcommand(s)
    import-key    Import age identity file
    key-gen       Generate new key
    list-keys     List available keys
    qrcode        Display a QR code to import a key into Cryptocam
```
