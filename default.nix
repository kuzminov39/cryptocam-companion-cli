{ pkgs ? import <nixpkgs> {}, ... }:


let
  naerskSource = import (pkgs.fetchFromGitHub {
    repo = "naersk";
    owner = "nmattia";
    rev = "b3b099d669fc8b18d361c249091c9fe95d57ebbb";
    sha256 = "156fbnr5s2n1xxbbk2z9xa7c5g2z5fdpqmjjs6n9ipbr038n0z3s";
  });
  naersk = pkgs.callPackage naerskSource {};
in naersk.buildPackage {
  name = "cryptocam-cli";
  version = "0.1.1";
  root = ./.;
  # our only external dependency is ffmpeg; pkg-config
  # is just needed so that cargo can find it
  buildInputs = with pkgs; [ ffmpeg pkg-config ];
}
